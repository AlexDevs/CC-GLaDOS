_G.cmdblock = peripheral.find("command")
_G.chatbox = peripheral.find("chatbox_admin")
_G.monitor = peripheral.find("monitor")
_G.modem = peripheral.find("modem")
_G.speaker = peripheral.find("speaker")
local usePing = false -- use "ping" (essentials command) for say and tell
local cc175 = false -- If you're using cc1.75 or upper

os.loadAPI("apis/config")
os.loadAPI("apis/db")
os.loadAPI("apis/task")

function crash(err)
	local cmdblock = peripheral.find("command")
    for k,v in pairs(db.list("whitelist") or {}) do
        if cmdblock then
            cmdblock.setCommand("/tell "..k.." GLaDOS 1.4 failed: "..err or "undefined error")
            cmdblock.runCommand()
        end
    end
    local sensor = peripheral.find("playerSensor")
    if sensor then
        local tabPl = {}
        local players = sensor.getNearbyPlayers(16)
        for k, v in pairs(players) do
            table.insert(tabPl,v.player)
        end
        
        for k, v in pairs(db.list("whitelist") or {}) do
            if cmdblock then
                cmdblock.setCommand("/tell "..k.." GLaDOS 1.4: Nearby players: "..table.concat(tabPl,", "))
                cmdblock.runCommand()
            end
        end
    end
	local function drawbsod()
		term.setTextColor(colors.white)
		term.setBackgroundColor(colors.blue)
		pcall(term.current().setTextScale, 1)
		term.clear()
		local w, h = term.getSize()
		term.setCursorPos(math.ceil(w/2)-5,2)
		term.setTextColor(colors.blue)
		term.setBackgroundColor(colors.white)
		print(" GLaDOS ")
		term.setTextColor(colors.white)
		term.setBackgroundColor(colors.blue)
		term.setCursorPos(1, 4)
		print("    An error has occured and the program has")
		print("    been shut down.")
		print("")
		print("    "..err)
		print("")
		print("    To fix this problem:")
		print("    - Reboot the computer.")
		print("")
		local s = "Press any key to continue"
		local x, y = term.getCursorPos()
		term.setCursorPos(math.ceil(w/2)-math.floor(s:len()/2), h-1)
		print(s)
	end
	if monitor then
		term.redirect(monitor)
		drawbsod()
	end
	term.redirect(term.native())
	drawbsod()
	os.pullEvent("key")
	os.reboot()
end

local function main() --no tabs

if not cmdblock then
	crash("Missing command block")
elseif not chatbox then
	crash("Missing chatbox admin")
elseif not monitor then
	crash("Missing monitor")
end

function os.version()
	return "GLaDOS 1.4"
end
 
function os.build()
	return 9
end


function _G.logs(txt,col)
	term.redirect(monitor)
	term.setTextColor(col or colors.white)
	term.setBackgroundColor(colors.black)
	print(txt)
	term.redirect(term.native())
end

function _G.convert(msg)
    if usePing then
        msg = msg:gsub("\\&","&&")
    elseif cc175 then
        msg = msg:gsub("&", "\167")
    	msg = msg:gsub("\\\167", "&")
    else
    	msg = msg:gsub("&", "\194\167")
    	msg = msg:gsub("\\\194\167", "&")
    end
	return msg
end

function _G.exec(command,nolog)
	command = convert(command)
	cmdblock.setCommand(command)
	cmdblock.runCommand()
	if not nolog then
		logs(command)
	end
end

function _G.say(msg)
	msg = convert(msg)
	if usePing then
		exec("sudo @a ping "..convert("&d[&a&lGLa&b&lDOS&d 1.4] ")..msg,true)
	else
		exec("say "..msg,true)
	end
	if speaker then
	    local spmsg = msg:gsub("&","")
	    if cc175 then
	        local spmsg = msg:gsub("\167","")
	    else
	     local spmsg = msg:gsub("\194\167","")
	    end
	    speaker.speak(spmsg)
	end
	logs("* "..msg)
end

function _G.tell(player, msg)
	msg = convert(msg)
	if usePing then
		exec("sudo "..player.." ping "..convert("&a&lGLa&b&lDOS &d1.4 &7&owhispers to you: ")..msg,true)
	else
		exec("tell "..player.." "..msg,true)
	end
	logs("To "..player..": * "..msg,colors.lightGray)
end

function string.split(str, sep)
  local out = {}
  if sep == nil then
    sep = "%s"
  end
  i=1
  for st in string.gmatch(str, "([^"..sep.."]+)") do
    out[i] = st
    i = i + 1
  end
  return out
end

term.redirect(monitor)

term.setBackgroundColor(colors.black)
term.setTextColor(colors.white)
term.clear()
term.setCursorPos(1,1)

term.redirect(term.native())
logs("GLaDOS 1.4")
say("All systems running!")

if not modem then
	logs("! Missing Modem !")
else
	modem.open(3165)
end

local cores = {}

task.add(function()
	for _, v in ipairs(peripheral.getNames()) do --load cores
		if peripheral.getType(v) == "drive" and disk.hasData(v) then
			os.queueEvent("disk",v)
		end
	end

	while true do
		local ev = {os.pullEvent()}
		
		local fullCMD
		local player
		local cmd
		local scmd
		
		if ev[1] == "chatbox_command" then
			fullCMD = ev[4]
			player = ev[3]
		elseif ev[1] == "modem_message" then
			local cmdm = ev[5]
			player = db.get("ids",rch) or "Ale32bit"
			fullCMD = "glados "..cmdm.line
		end
		
		if fullCMD ~= "" and fullCMD then
			fullCMD = string.split(fullCMD," ")
				
			if fullCMD[1]:lower() == "glados" and db.get("whitelist",player) then
				scmd = {}
				for i = 2,#fullCMD do
					table.insert(scmd,fullCMD[i])
				end
				cmd = table.concat(scmd," ")
				
				if cmd:sub(1,8) == "/script " then
					logs("LUA: "..cmd:sub(9),colors.lime)
					local pid = task.add(function()
						local ok, err = pcall(function() setfenv(loadstring(cmd:sub(9)),getfenv())() end)
						if not ok then
							logs("LUA ERROR: "..err,colors.red)
							tell(player,"[Script] Error: "..err)
						end
					end, player.."'s lua script")
					logs("Script PID "..pid.." running",colors.lime)
				elseif cmd:sub(1,13) == "/updateglados" then
					say("&aUpdating OS...")
					local function save(url, file)
						local h = http.get(url).readAll()
						local f = fs.open(file,"w")
						f.write(h)
						f.close()
					end
					save("https://gitlab.com/Ale32bit/CC-GLaDOS/raw/master/startup.lua","startup")
					save("https://gitlab.com/Ale32bit/CC-GLaDOS/raw/master/apis/config.lua","apis/config")
					save("https://gitlab.com/Ale32bit/CC-GLaDOS/raw/master/apis/db.lua","apis/db")
					save("https://gitlab.com/Ale32bit/CC-GLaDOS/raw/master/apis/task.lua","apis/task")
					
					os.reboot()
				elseif cmd:sub(1, 6) == "/tell " then
					cmd = string.split(cmd)
					local toPlayer = cmd[2]
					local ncmd = {}
					for i = 3,#cmd do
						table.insert(ncmd,cmd[i])
					end
					ncmd = table.concat(ncmd," ")
					tell(toPlayer, ncmd)
				elseif cmd:sub(1,1) == "/" then
					exec(cmd)
				else
					if cmd ~= "" then
						say(cmd)
					end
				end
			end
		end
	end
end, "GLaDOS")

task.add(function()
	while true do
		local ev = {os.pullEvent()}
		if ev[1] == "disk" then
			local v = ev[2]
			if peripheral.getType(v) == "drive" and disk.hasData(v) then
				if fs.exists(fs.combine(disk.getMountPath(v),"coreAdd")) then
					logs(v.." loaded.",colors.lightBlue)
					cores[v] = task.add(function()
						local ok, err = pcall(function()
							setfenv(loadfile(fs.combine(disk.getMountPath(v),"coreAdd")),setmetatable({
								chat = function(msg)
									local name = config.read(fs.combine(disk.getMountPath(v),"conf"),"name") or "undefined"
									local ch = "&d["..name.."&d] "..msg
									logs(ch,colors.lightBlue)
									say(ch)
								end,
								coreName = function()
									return config.read(fs.combine(disk.getMountPath(v),"conf")) or "undefined"
								end,
								corePath = function()
									return disk.getMountPath(v)
								end,
								getCmdBlock = function()
									return cmdblock
								end,
								getMonitor = function()
									return term.native()
								end,
								getCoreType = function()
									return "GLaDOS"
								end,
								getCoreVersion = function()
									return os.version()
								end,
								getCoreBuild = function()
									return os.build()
								end,
							}, {__index = getfenv()}))()
						end)
						local conf = {}
						conf = config.list(fs.combine(disk.getMountPath(v),"conf"))
						if not ok then
							say("Guess what, "..(conf.name or "a core").." just died.")
							say("It left this message: "..err)
						end
					end, "CORE: "..config.list(fs.combine(disk.getMountPath(v),"conf")).name or v.." - "..disk.getPath(v))
				end
			end
		elseif ev[1] == "disk_eject" then
			logs(ev[2].." removed.",colors.lightBlue)
			task.kill(ev[2])
			cores[ev[2]] = nil
		end
	end
end,"Auto Core Loader")

task.run()
end

local ok,err = pcall(main)

if not ok then
	crash(err)
end